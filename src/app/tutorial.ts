
export class Tutorial {
    id: number;
    nombre: string;
    descripcion: string;
    imagen?: any;
    body?: any;
}
